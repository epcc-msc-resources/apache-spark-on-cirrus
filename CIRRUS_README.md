# Apache Spark on Cirrus

Login to Cirrus.

On the login node, load the required module for Anaconda Python:
```
module load anaconda/python3
```

Create a new anaconda environment with Python 3.10 (*Note*: your SAFE_PROJECT_ID is something like 'm22oc' or 'm22ext',
have a look at the project(s) you are a member of in https://safe.epcc.ed.ac.uk if you are not sure:)
```
cd /work/[SAFE_PROJECT_ID]/[SAFE_PROJECT_ID]/[STUDENT_ID]
mkdir -p .conda/envs
conda create --prefix .conda/envs/py310 python=3.10 jupyter openjdk
eval "$(${ANACONDADIR}/bin/conda shell.bash hook)"
source activate .conda/envs/py310
```

You only need to create the conda environment once. 
From now on you can activate it as follows:
```
cd /work/[SAFE_PROJECT_ID]/[SAFE_PROJECT_ID]/[STUDENT_ID]
eval "$(${ANACONDADIR}/bin/conda shell.bash hook)"
source activate .conda/envs/py310
```

Download and unpack Spark (we are using release 3.2.2 from 15 July 2022):
```
wget https://downloads.apache.org/spark/spark-3.2.2/spark-3.2.2-bin-hadoop3.2.tgz
tar xzf spark-3.2.2-bin-hadoop3.2.tgz
```

Now clone this repository containing the lab notebooks:
```
git clone https://git.ecdf.ed.ac.uk/akrause/apache-spark-on-cirrus
```

Refer to the Cirrus documentation on how to run an interactive job:
https://cirrus.readthedocs.io/en/master/user-guide/batch.html#interactive-jobs

In this example, we allocate 1 node for 20 minutes and log in:
```
$ salloc --exclusive --nodes=1 --time=00:20:00 --partition=standard --qos=standard --account=[account]
salloc: Granted job allocation 206209
salloc: Waiting for resource configuration
salloc: Nodes r1i1n17 are ready for job

$ ssh r1i1n17
```
After logging in to the node, load the conda module and activate the environment,
and set `JAVA_HOME` and `SPARK_HOME`:
```
module load anaconda/python3
source activate /work/[SAFE_PROJECT_ID]/[SAFE_PROJECT_ID]/[STUDENT_ID]/.conda/envs/py310
export SPARK_HOME=/work/[SAFE_PROJECT_ID]/[SAFE_PROJECT_ID]/[STUDENT_ID]/spark-3.2.2-bin-hadoop3.2/
export HOME=/work/[SAFE_PROJECT_ID]/[SAFE_PROJECT_ID]/[STUDENT_ID]
```
Then start PySpark with Jupyter:
```
PYSPARK_DRIVER_PYTHON=jupyter PYSPARK_DRIVER_PYTHON_OPTS="notebook --ip=0.0.0.0 --allow-root --notebook-dir $HOME/apache-spark-on-cirrus/notebooks" $SPARK_HOME/bin/pyspark
```
This allows connections from other hosts than localhost which you will need when 
you connect from your local browser.
It may take a moment to start up but eventually you should see something like this:
```
[C 15:24:28.857 NotebookApp] 
    
    To access the notebook, open this file in a browser:
        file:///lustre/home/y15/akrause2/.local/share/jupyter/runtime/nbserver-36043-open.html
    Or copy and paste one of these URLs:
        http://r1i6n20:8888/?token=637dab26d98e014414b414892103493d0bdf7eef6ed07729
     or http://127.0.0.1:8888/?token=637dab26d98e014414b414892103493d0bdf7eef6ed07729
```


In another shell log in to Cirrus again, to enable port forwarding for Jupyter:
```
ssh <USERNAME>@login.cirrus.ac.uk -L8888:<MASTER_NODE>:8888
```
Here, <MASTER_NODE> is the name of the Cirrus node where your interactive job is 
currently running (e.g. `r1i1n17` in the above example)

Copy the token from above and point your local browser to:
http://localhost:8888/?token=TOKEN
(or http://localhost:8888 and type in the token when requsted).

This should show you a Jupyter home page.

Now you can create a new notebook with Python3 and start the exercise in
[practical-intro.md](practical-intro.md).
