# Apache Spark - Fundamentals of Data Management

## Introduction

This repository provides the materials for the practical lab exercises for the two Spark practicals in the Fundamentals of Data Management module.

## Setup

In order to complete the practical exercises (and the assessment on Spark), you will need to have a Spark
environment to work in. There are a variety of methods for setting this up, as described in the sections below.
The simplest way is to the EIDF Notebook Service, which has all the dependencies already installed. The second 
method uses an Anaconda environment on your own PC, which can then be used to launch a 'local' Spark cluster
through the Python API. The third method uses Cirrus to launch the Spark cluster and
Jupyter notebook server.

After setting up your environment, try to complete the exercise at [practical-intro.md](./practical-intro.md). You might want to work through the following Notebooks first, making sure you understand what is happening at each stage.

 - Spark Lab 1 Notebook: [notebooks/lab1_basics.ipynb](notebooks/lab1_basics.ipynb)
 - Spark Lab 2 Notebook: [notebooks/lab2_1_dataframes.ipynb](notebooks/lab2_1_dataframes.ipynb)
 - if you finish all the above, then you might want to work through [notebooks/lab_rdd_dataframes_NLTK.ipynb](notebooks/lab_rdd_dataframes_NLTK.ipynb)


### Method 1 - EIDF Noteboook Service

1. Go to https://notebook.eidf.ac.uk, and click "Sign in with SAFE"
2. On the next page, if you happen to have more than one EIDF account, make sure you select your MSc one from the "User Account" dropdown menu, and then click Accept.
3. Select the "Apache Spark" Server Option, and click "Start". This will launch a Jupyter Lab session. 
4. From the Launch Window, launch either a Python or R Notebook depending on your preference.
5. If you use Python, the Notebook has all the libraries required, but you will need to run some initialisation code:

   ```
   import os
   import sys
   
   # Import SparkSession
   from pyspark.sql import SparkSession, SQLContext
   spark = SparkSession.builder.master("local[4]").appName("FDM_practical").getOrCreate()
   sc = spark.sparkContext

   # The sqlContext object is only required for next week's lab
   sqlContext = SQLContext(spark)
   ```

6. clone the repository for the practical so that you have the data files needed:

   In a cell, type (or copy and paste):

   ```
   !git clone https://git.ecdf.ed.ac.uk/epcc-msc-resources/apache-spark-on-cirrus
   ```

7. You can then start the exercise in [practical-intro.md](./practical-intro.md). When reading in the files (e.g. when creating the `textfile` variable) you will need to
include the full path to the file.

### Method 2 - Local Spark Cluster on Your Own PC
1. Ensure you have the Conda package manager available for managing Python software libraries:
   1. **EITHER** install Anaconda if you want a full Anaconda scientific Python ecosystem and GUI:
      1. Download the latest Anaconda for Windows, Mac or Linux from https://www.anaconda.com/
      2. Follow the installation instructions for your platform at https://docs.anaconda.com/anaconda/install/
   2. **OR** install Miniconda if you only want to use Conda package manager from the command line interface (CLI):
      1. Download the latest Miniconda for Windows, Mac or Linux from https://docs.conda.io/en/latest/miniconda.html
      2. Follow the installation instructions for your platform at https://docs.conda.io/en/latest/miniconda.html#installing
2. Create a new `Python 3.10` virtual environment in
   [Anaconda Navigator](https://docs.anaconda.com/navigator/tutorials/manage-environments/)
   or in [Conda](https://conda.io/projects/conda/en/stable/user-guide/tasks/manage-environments.html), for example:
   ```commandline
   conda create -p <PATH_TO_VIRTUAL_ENV>/spark python=3.10
   ```
3. Activate the newly created virtual environment:

   ```commandline
   conda activate <PATH_TO_VIRTUAL_ENV>/spark
   ```

4. Install the JupyterLab, Spark, OpenJDK and Numpy packages from the `conda-forge` channel in the Anaconda package repository:

   ```commandline
   conda install -c conda-forge jupyter jupyterlab pyspark numpy openjdk nltk
   ```

   ***Note***: You may also install the [Mamba](https://mamba.readthedocs.io/en/latest/user_guide/mamba.html) package
   manager **before** you run the above command because it is a lot faster at installing packages than default `conda`.
   To install Mamba, use the command: `conda install -c conda-forge mamba` between steps 3 and 4 above.
5. With these steps complete, clone this repository - `git clone https://git.ecdf.ed.ac.uk/epcc-msc-resources/apache-spark-on-cirrus.git`

#### Mac and Linux Users

It is possible to launch the Jupyter Notebook server and specify the required environment variables to initialise the
Spark session at the same time with the following commands:

```commandline
export SPARK_HOME=<PATH_TO_VIRTUAL_ENV>/lib/python3.10/site-packages/pyspark/
PYSPARK_DRIVER_PYTHON=jupyter PYSPARK_DRIVER_PYTHON_OPTS=lab pyspark
```
This will generate output similar to the following:
```
[I 2023-11-14 15:29:05.594 ServerApp] Jupyter Server 1.24.0 is running at:
[I 2023-11-14 15:29:05.594 ServerApp] http://localhost:8888/lab?token=d97b8c8b5b96fd60262f126ca2c223cfb46a805d17a27b11
[I 2023-11-14 15:29:05.594 ServerApp]  or http://127.0.0.1:8888/lab?token=d97b8c8b5b96fd60262f126ca2c223cfb46a805d17a27b11
[I 2023-11-14 15:29:05.594 ServerApp] Use Control-C to stop this server and shut down all kernels (twice to skip confirmation).
[C 2023-11-14 15:29:05.628 ServerApp] 
   
    To access the server, open this file in a browser:
        file:///home/user/Library/Jupyter/runtime/jpserver-77152-open.html
    Or copy and paste one of these URLs:
        http://localhost:8888/lab?token=d97b8c8b5b96fd60262f126ca2c223cfb46a805d17a27b11
     or http://127.0.0.1:8888/lab?token=d97b8c8b5b96fd60262f126ca2c223cfb46a805d17a27b11
```

This will initialise the PySpark context (`sc` in the exercises) with the required paths and launch a notebook server at the same time.

Copy the URL and token from above and point your local browser to: http://localhost:8888/?token=TOKEN (or http://localhost:8888 and type in the token when requested).
This should show you a Jupyter home page. Now you can create a new notebook with Python3 and start the exercise in [practical-intro.md](./practical-intro.md).

#### Windows users
Start a Jupyter Notebook server with the following command, and then you can create
   a new `Python 3` kernel notebook with your own code for the assessment or open the existing notebooks for the practical
   (and follow the instructions in [practical-intro.md](./practical-intro.md):
   ```commandline
   jupyter lab --notebook-dir <PATH_TO_THIS_DIRECTORY>/notebooks
   ```
   This will generate output similar to the following:
   ```commandline
   [I 09:16:56.223 NotebookApp] Serving notebooks from local directory: ./spark_assessment
   [I 09:16:56.223 NotebookApp] Jupyter Notebook 6.5.1 is running at:
   [I 09:16:56.224 NotebookApp] http://localhost:8888/?token=b8789a49f6ce64c27d3722c73fb52c93c7ddd5c1c98ef161
   [I 09:16:56.225 NotebookApp]  or http://127.0.0.1:8888/?token=b8789a49f6ce64c27d3722c73fb52c93c7ddd5c1c98ef161
   [I 09:16:56.225 NotebookApp] Use Control-C to stop this server and shut down all kernels (twice to skip confirmation).
   [C 09:16:56.357 NotebookApp]
   
       To access the notebook, open this file in a browser:
           file:///home/user/jupyter/runtime/nbserver-23668-open.html
       Or copy and paste one of these URLs:
           http://localhost:8888/?token=b8789a49f6ce64c27d3722c73fb52c93c7ddd5c1c98ef161
        or http://127.0.0.1:8888/?token=b8789a49f6ce64c27d3722c73fb52c93c7ddd5c1c98ef161
   ```
   Copy the token from above and point your local browser to: http://localhost:8888/?token=TOKEN
   (or http://localhost:8888 and type in the token when requsted).
   This should show you a Jupyter home page.    Now you can create a new notebook with Python3 and start the exercise in
   [practical-intro.md](./practical-intro.md).


Once you have created or opened a notebook for your code, you may then launch a local Spark cluster - note that the `x` value
   in square brackets in `local[x]` represents the number of cores you wish to allocate to the Spark cluster - with the
   following code in a cell at the top of the notebook, and you can reuse the `spark` and `sc` spark context object for
   all API commands you need:
   ```commandline
   import os
   import sys
    
   os.environ['PYSPARK_PYTHON'] = sys.executable
   os.environ['PYSPARK_DRIVER_PYTHON'] = sys.executable
    
   # Import SparkSession
   from pyspark.sql import SparkSession, SQLContext
   spark = SparkSession.builder.master("local[4]").appName("FDM_practical").getOrCreate()
   sc = spark.sparkContext

   # sqlContext object is required for Lab 2 notebooks
   sqlContext = SQLContext(spark)
   ```
   
Please find more information about installing and running Jupyter and the Notebook server
[here](https://jupyterlab.readthedocs.io/en/stable/getting_started/installation.html) and
[here](https://docs.jupyter.org/en/latest/running.html).

### Method 3 - Spark on Cirrus

Please follow these [Cirrus instructions](./CIRRUS_README.md) for more details on how to setup your Spark environment on Cirrus
You may also refer to the Cirrus [Spark cluster instructions](./cluster/README.md) for more information.

