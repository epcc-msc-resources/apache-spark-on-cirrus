Following the Hadoop practical from last week, we will re-implement the example in Spark. First, create an RDD from the local text file "StarWars.txt" as we have seen in the lecture. For example:
```
textfile = sc.textFile("StarWars.txt")
```

What happens? How does the dataset look like? Remember that you can look at the contents of an RDD by calling `collect()`.

The goal is to implement the same functionality as you did in the Hadoop MapReduce practical and count the words in a file or a set of files.
First, we have to plan the transformations and actions to apply. The map function splits the input data - one line of a text file - into a list of words and outputs a list of pairs ( *[WORD]*, 1). Have a look at the Spark transformations "map", "flatMap" and the action "reduceByKey". Remember that you can always look at intermediate results by calling "collect()" on an RDD. The goal is to sum up the occurrences of each word.

There are a few more text files in the data directory of this repository.

A few more ideas for processing once you have achieved the simple word count:

 * Sort the words by the number of times that they occur
 * Filter by word length and ignore all words with less than 3 letters
 * Only return words that occur more than 100 times
 * Only return the top 10 words

If you finished the practical you may want to look at the [RDD Programming Guide](https://spark.apache.org/docs/latest/rdd-programming-guide.html) or the [SQL Programming Guide](https://spark.apache.org/docs/latest/sql-programming-guide.html) for more details. 

If you have access to a Hadoop cluster, Spark can also read files stored in HDFS. For this to work you have to point PySpark to the configuration file of a Hadoop cluster. This is done by setting the environment variable HADOOP_CONF_DIR to the configuration directory of the Hadoop cluster (or a local copy of this), just as you do when working with Hadoop directly.

If you are interested to learn more about Spark in your own time, more advanced exercises can be found in other exercises. We will continue with these exercises next week.
